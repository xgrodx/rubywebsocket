require "base64"
require "socket"
require "digest/md5"
require "digest/sha1"
require "stringio"
require "./server/User"

class WebSocketServer

	MAGIC_NUMBER = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

	def initialize(port)
		@port = port
		@server = TCPServer.open(@port)
		@users = Array.new
		@userId = 0
		puts "Server started at port #{@port}\n"
	end

	def run
		while true	
			Thread.start(@server.accept) do |socket|
				user = User.new
				user.socket = socket
				@users.push(user)
				user.userId = @userId
				@userId += 1
				puts "Client connected"

				while (!user.socket.eof?)
					yield user, readUserData(user)
				end
				killUser(user)
			end
		end
	end

	def sendHandShake(user)
		# Read Header
		puts "Read header"
		header = {}
		while line = user.socket.gets($/)
			line = line.chomp()
			puts line+"\n"
			break if line.empty?
			if !(line =~ /\A(\S+): (.*)\z/n)
				puts("Error in header")
			end
			header[$1] = $2
		end
	
		puts("Send handshake")
	
			if header["Sec-WebSocket-Version"]
				user.socketVersion = header["Sec-WebSocket-Version"]
				securityBytes = ""
				key = header["Sec-WebSocket-Key"]
				acceptString = "Sec-WebSocket-Accept: " + Base64.encode64(Digest::SHA1.digest(key + MAGIC_NUMBER)).gsub(/\n/, "") + "\r\n"
				puts "Accept string = " + acceptString
			else
				user.socketVersion = "hixie-76"
				clientKey1 = header["Sec-WebSocket-Key1"]
				clientKey2 = header["Sec-WebSocket-Key2"]
				clientKey3 = user.socket.read(8) # 8 last bytes
				securityBytes = Digest::MD5.digest(getBytes(clientKey1) + getBytes(clientKey2) + clientKey3)
				acceptString = ""
			end
			
			user.socket.write(
				"HTTP/1.1 101 WebSocket Protocol Handshake\r\n"+
				"Upgrade: WebSocket\r\n"+
				"Connection: Upgrade\r\n"+
				"Sec-WebSocket-Origin: "+header["Origin"]+"\r\n"+
				"Sec-WebSocket-Location: ws://"+ header["Host"] +"/\r\n"+
			acceptString + "\r\n" + 
			securityBytes
			)

		user.socket.flush()
		user.handshaked = true

	end

	def getBytes(key)
		num = key.gsub(/[^\d]/n, "").to_i() / key.scan(/ /).size
		return [num].pack("N")
	end
	
	def readUserData(user)
		line = ""
		if (!user.handshaked || user.socketVersion == "hixie-76")
			line = user.socket.gets
		elsif user.socketVersion == ""
			line = user.socket.gets
		else
			# Read frame packet
			begin	
				bytes = user.socket.read(2).unpack("C*")
				fin = (bytes[0] & 0x80) != 0
				opcode = bytes[0] & 0x0f
				mask = (bytes[1] & 0x80) != 0
				plength = bytes[1] & 0x7f
				if plength == 126
					bytes = user.socket.read(2)
					plength = bytes.unpack("n")[0]
				elsif plength == 127
					bytes = user.socket.read(8)
					(high, low) = bytes.unpack("NN")
					plength = high * (2 ** 32) + low
				end

				if !mask
					puts "No masking. Not supported\n"
					return ""
				end
				maskKey = mask ? user.socket.read(4).unpack("C*") : nil
				payload = user.socket.read(plength)
				payload = applyMask(payload, maskKey) if mask
				if (opcode == 0x01) # opcode text. TODO : Implement binary etc
					line = encode(payload, "UTF-8")
				end
			rescue EOFError # error reading socket
				line = ""
			end
		end

		if line =~ /GET (\S+) HTTP\/1.1/
			sendHandShake(user)
		end
		return line #return last line
	end

	def encode(str, encoding)
		str.force_encoding( encoding )
	end

	def applyMask(payload, maskKey)
		origBytes = payload.unpack("C*")
		newBytes = []
		origBytes.each_with_index() do |b, i|
			newBytes.push(b ^ maskKey[i % 4])
		end
		return newBytes.pack("C*")
	end

	def killUser(user)
		multicast(user, "/disconnect#{user.userId}")
		@users.delete(user)
		user.socket.close
		puts "Client disconnected"
	end

	def multicast(sender, message)
		puts "Server multicast: #{message}"
		@users.each do |user|
			if user != sender
				if user.socketVersion == "hixie-76"
					user.socket.write "\x00#{message}\xff"
				elsif user.socketVersion == ""
					user.socket.write message
				else
					sendFrame user, message
					#user.socket.write message
				end
				user.socket.flush
			end
		end
	end

	def sendFrame(user, message)
		buffer = StringIO.new
		writeByte buffer, 0x81
		# Andra prylar om paket-datans storlek > 125
		#if message.bytesize <= 125
			writeByte buffer, message.bytesize
		#end
		buffer.write message
		user.socket.write buffer.string
	end

	def writeByte(buffer, byte)
		buffer.write([byte].pack("C"))
	end
end
